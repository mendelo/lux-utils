import wrap from './wrap';

/**
 * Builds a view delegate wrapped with a post-hook method tied to switchView
 * @param {builder: { build: Function, postHook: Function} } opts 
 */
function buildViewDelegate({ builder, postHook }) {
    const view = builder.build();
    wrap(view, builder.switchView(view)).with({ post: postHook });
    return view;
}

/**
 * A-B View Container
 * 
 * @description Creates an "A"/"B" view that can be switched on an existing function.
 * Views are re-created on each renderView call.
 */
class ABView {
    /**
     *  A-B View Constructor
     * 
     * @param {builder {aViewBuilder, bViewBuilder}}
     * aViewBuilder and bViewBuilder should contain: { builder: { build: Function to return the view, postHook: Function to attach view switch }}.
     * 
     * @param {opts {render}}
     * The render method responsible for the render cycle, including any teardown, cleanup, and framework-specific event cycles.
     */
    constructor({ aViewBuilder, bViewBuilder, render }) {
        this.aViewBuilder = aViewBuilder;
        this.bViewBuilder = bViewBuilder;
        this.render = render;
    }

    /**
     * Render a new view through the render cycle, containing the post-hook switcher.
     * @param {'a'|'b'} type 'a' to render the 'A' view, 'b' to render the 'B' view.
     * @exception An exception is thrown if type is not lowercase 'a' or lowercase 'b'.
     */
    renderView(type) {
        let view;
        switch (type) {
            case 'a':
                view = buildViewDelegate({ builder: this.aViewBuilder, postHook: () => this.render(this.renderView('b')) })
                break;
            case 'b':
                view = buildViewDelegate({ builder: this.bViewBuilder, postHook: () => this.render(this.renderView('a')) })
                break;
            default:
                throw `Unsupported view type: ${type}. Supported types are 'a' or 'b'.`;
        }
        return view;
    }
}

export default ABView;