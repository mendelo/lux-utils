/**
 * Wrap a function and object with a pre/post-execute callback
 * 
 * @description Wraps a function and calls a pre/post-execute callback using the original caller scope.
 * Caller scope can be modified by calling with call/bind/apply.
 * 
 * @usage Example: Wrap an object in its own scope with both pre and post callbacks
 * @example wrap(a, a.method).with({ pre: name => { before(); }, post: name => { after(); } });
 * 
 * @usage Example: Wrap an object with a prototype or object property using only a pre-execute callback
 * @example wrap(log_instance, Logger.log).with({ pre: () => { log_instance.log(Date.now()); } })
 * 
 * @usage Example: Wrap an object using only a post callback
 * @example wrap(person, person.greet).with({ post: name => console.log(`Nice to meet you, ${name}.`) });
 * 
 * @param {any} targetObject The target object to apply the callback scope
 * @param {Function} targetFunction The function to wrap
 */
export default function wrap(targetObject, targetFunction) {
    const scope = this;
    const originalFunction = targetFunction;
    return {
        with: function ({ pre, post }) {
            targetObject[originalFunction.name] = function () {
                if (pre) {
                    pre.apply(scope, arguments);
                }
                originalFunction.apply(scope, arguments);
                if (post) {
                    post.apply(scope, arguments);
                }
            }
        }
    }
}